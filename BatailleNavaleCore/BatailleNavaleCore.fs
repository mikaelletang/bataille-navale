module BatailleNavaleCore

type Navire = 
    | PorteAvion
    | Croiseur
    | ContreTorpilleur
    | SousMarin
    | Torpilleur

type Direction = 
    | Verticale
    | Horizontale

type Dimension = 
    {
        largeur: int
        hauteur: int }

let longueur = 
    function 
    | PorteAvion -> 5
    | Croiseur -> 4
    | ContreTorpilleur -> 3
    | SousMarin -> 3    
    | Torpilleur -> 2

module Coordonnée =
    type T = char*int
    
    let private calculeHauteurMax dimension :char = char ((int 'A') + dimension.hauteur - 1)
    
    let create dimension = function
    | (_, ligne) when ligne < 1 -> None
    | (_, ligne) when ligne > dimension.largeur -> None
    | (colonne, _) when colonne > calculeHauteurMax dimension -> None
    | c -> Some c 
        

type PositionNavire = 
    { navire : Navire
      coordonnée : Coordonnée.T
      direction : Direction }

type Grille = 
    { dimension : Dimension
      positionNavires : PositionNavire list }

let placerUnNavire grille positionNavire = { grille with positionNavires = positionNavire :: grille.positionNavires }
