module BatailleNavaleCore.Tests

open Expecto

let dimension = {
  largeur=10;
  hauteur=10;
}

let grille = {
  dimension=dimension;
  positionNavires=[];
}

[<Tests>]
let tests =
  testList "Bataille Navale" [
    testList "coordonnées" [    
      testCase "ligne avant la grille" <| fun _ ->
        let coordonnée = Coordonnée.create dimension ('A', 0)
        Expect.isNone coordonnée "La coordonnée est considérée valide"
        
      testCase "colonne avant la grille" <| fun _ ->
        let coordonnée = Coordonnée.create dimension ('a', 1)
        Expect.isNone coordonnée "La coordonnée est considérée valide"
        
      testCase "ligne après la grille" <| fun _ ->
        let coordonnée = Coordonnée.create dimension ('A', 11)
        Expect.isNone coordonnée "La coordonnée est considérée valide"
        
      testCase "colonne après la grille" <| fun _ ->
        let coordonnée = Coordonnée.create dimension ('A', 11)
        Expect.isNone coordonnée "La coordonnée est considérée valide"
    ]
    
    testList "placements" [
      testCase "On peut placer un Navire sur la grille" <| fun _ ->
        let direction = Verticale
        let coordonnée = (Coordonnée.create dimension ('A', 1)).Value
        
        let positionPorteAvion = {
          navire=PorteAvion;
          coordonnée=coordonnée;
          direction=direction;
        } 
        
        let nouvelleGrille = placerUnNavire grille positionPorteAvion
        
        Expect.contains nouvelleGrille.positionNavires positionPorteAvion "La grille ne contient pas le porteAvion"
        
      testCase "On ne peut pas placer un Navire qui sort de la grille" <| fun _ ->
        let direction = Verticale
        let coordonnée = (Coordonnée.create dimension ('J', 1)).Value
        
        let positionPorteAvion = {
          navire=PorteAvion;
          coordonnée=coordonnée;
          direction=direction;
        } 
        
        let nouvelleGrille = placerUnNavire grille positionPorteAvion
        
        Expect.isNone nouvelleGrille
    ]
  ]
